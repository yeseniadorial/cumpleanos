# Aplicación desarrollada con Jakarta EE 8 | PayaraMicro

## Introducción
Esta aplicación esta desarrollada con JAVA, se utiliza PayaraMicro para autocontener la aplicación, el Front esta desarrollado con JSP

para mas información sobre PayaraMicro visite: https://www.payara.fish/products/payara-micro/

Se utilizan los pipelines de Bitbucket para realizar CI/CD de la aplicaión al PaaS Heroku.

**Nota: debe tener instalado Maven** 

Paso 1. El comando para generar el ejecutable es:
   mvn install payara-micro:bundle

Paso 2. Esto creará un fichero ejecutable jar **diasCumple-1-microbundle.jar** dentro de la carpeta _target_. El cual se ejecuta de la siguiente forma:

    java -jar target/diasCumple-1-microbundle.jar --port 9595




Paso 3. Para probar la aplicación, abra su navegador con la siguiente URL:

    http://localhost:9595/

## Despliegue continuo

La aplicación esta desplegada en Heroku, se puede aceder con la siguiente URL:
https://dias-cumple.herokuapp.com/

La URL del repositorio en Bitbucket es:
https://bitbucket.org/yeseniadorial/cumpleanos

**Nota:** Despues del primer request se debe esperar que Heroku levante la aplicación y volver a intentar refrescando la pagina.
 Esto se debe a que se usa la capa gratuita del servicio.














