package root.api.dto;

import java.time.LocalDate;

/**
 *
 * @author yesenia
 */
public class DTOPersona {

    private String nombreFull;
    private String nombre;
    private String apellido;
    private LocalDate fechaNacimiento;
    private Integer edad;
    private Integer diasParaCumpleanio;
    private String poema;

    public String getNombreFull() {
        return nombreFull;
    }

    public void setNombreFull(String nombreFull) {
        this.nombreFull = nombreFull;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getDiasParaCumpleanio() {
        return diasParaCumpleanio;
    }

    public void setDiasParaCumpleanio(Integer diasParaCumpleanio) {
        this.diasParaCumpleanio = diasParaCumpleanio;
    }

    public String getPoema() {
        return poema;
    }

    public void setPoema(String poema) {
        this.poema = poema;
    }

    @Override
    public String toString() {
        return "DTOCliente{" + "nombre1=" + nombre + ", apellido1=" + apellido + ", fechaNacimiento=" + fechaNacimiento + ", edad=" + edad + ", diasParaCumpleanio=" + diasParaCumpleanio + ", poema=" + poema + '}';
    }
    
    
}
