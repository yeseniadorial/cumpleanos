package root.api.resources;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import javax.json.JsonArray;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.api.dto.DTOPersona;

@Path("/")
public class Resource {

    @GET
    public Response ping() {
        return Response
                .ok("ping")
                .build();
    }

    @Path("datos/{nombrefull}/{fechaNacimiento}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN})
    public Response getDatos(@PathParam("nombrefull") String nombrefull, @PathParam("fechaNacimiento") String fechaNacimiento) {

        String[] nombreCompleto = nombrefull.split(",");

        DTOPersona persona = new DTOPersona();
        persona.setNombre(nombreCompleto[0].split(" ")[0]);
        persona.setApellido(nombreCompleto[1].split(" ")[0]);

        LocalDate nac = LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        LocalDate now = LocalDate.now();

        persona.setEdad((int) ChronoUnit.YEARS.between(nac, now));
        persona.setFechaNacimiento(nac);

        LocalDate next = LocalDate.of(now.getYear(), nac.getMonth(), nac.getDayOfMonth());

        if (next.isBefore(now)) {
            persona.setDiasParaCumpleanio((int) ChronoUnit.DAYS.between(now, next.plusYears(1)));
        } else {
            persona.setDiasParaCumpleanio((int) ChronoUnit.DAYS.between(now, next));
        }

        if (next.isEqual(now)) {
            Client client = ClientBuilder.newClient();
            WebTarget target = client.target("https://www.poemist.com/api/v1/randompoems");
            JsonArray response = target.request(MediaType.APPLICATION_JSON).get(JsonArray.class);

            int n = (int) Math.floor(Math.random() * response.size());

            String poema = response.getJsonObject(n).getString("content");
            String poeta = response.getJsonObject(n).getJsonObject("poet").getString("name");
            persona.setPoema("Felicidades por tu cumpleaños. Un poema para ti: \"".concat(poema).concat("\" Poeta: ").concat(poeta).concat("."));
        }
        return Response.ok(persona).build();
    }
}
