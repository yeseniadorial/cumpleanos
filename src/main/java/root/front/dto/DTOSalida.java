package root.front.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author yesenia
 */
public class DTOSalida implements Serializable{
    private String nombre;
    private String apellido;
    private LocalDate fechaNacimiento;
    private Integer edad;
    private Integer diasParaCumpleanio;
    private String poema;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getDiasParaCumpleanio() {
        return diasParaCumpleanio;
    }

    public void setDiasParaCumpleanio(int diasParaCumpleanio) {
        this.diasParaCumpleanio = diasParaCumpleanio;
    }

    public String getPoema() {
        return poema;
    }

    public void setPoema(String poema) {
        this.poema = poema;
    }

    public String getFechaString() {
        return fechaNacimiento.format(DateTimeFormatter.ofPattern("dd/MM/yy"));
    }

    @Override
    public String toString() {
        return "DTOSalida{" + "nombre=" + nombre + ", apellido=" + apellido + ", fechaNacimiento=" + fechaNacimiento + ", edad=" + edad + ", diasParaCumpleanio=" + diasParaCumpleanio + ", poema=" + poema + '}';
    }

}
