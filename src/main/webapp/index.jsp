<%-- 
    Document   : index.jsp
    Author     : yesenia
--%>

<%@page import="java.time.LocalDate"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

        <title>:)Cumpleaños:)</title>
    </head>
    <body>
        <!--<h1>¡Bienvenido!</h1>-->
        <h2>Ingrese los siguientes datos</h2>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div  id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.jsp">Inicio<span class="sr-only">(current)</span></a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="card mb-3">
<!--            <img src="https://shop.stockphotosecrets.com/stock-photo-preview/78355350/ing_42105_00085.jpg" >-->
            <div class="card-body">
                <h5 class="card-title">Ingreso de Datos</h5>
                

                <form action="controller" method="post">
                    <div class="form-group">
                        <label for="name="nombres"">Nombres</label>
                        <input type="text" name="nombres" class="form-control" id="nombres" placeholder="Nombres" required="true">
                    </div>
                    <div class="form-group">
                        <label for="apellidos">Apellidos</label>
                        <input type="text" name="apellidos" class="form-control" id="apellidos" placeholder="Apellidos" required="true">
                    </div>
                    <div class="form-group">
                        <label for="fecha">Fecha de Nacimiento</label>
                        <input type="date" name="fecha" class="form-control" id="fecha" required="true" max="<%=LocalDate.now()%>">
                    </div>
                    <div>
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Enviar</button>
                    </div>

                </form>
                
            </div>
        </div>
    </body>
    <footer class="page-footer font-small cyan darken-3">
        <div class="footer-copyright text-center py-3">© 2020 Copyright:
            <a href="https://www.linkedin.com/in/yeseniadoria/"> Yesenia Doria</a><br>
            <a href="https://bitbucket.org/yeseniadorial/cumpleanos"> Código App en Bitbucket</a><br>
            <a href="https://dias-cumple.herokuapp.com/"> App desplegada en Heroku</a>
            
        </div>
    </footer>
</html>
