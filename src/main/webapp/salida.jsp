<%-- 
    Document   : salida
    Author     : yesenia
--%>

<%@page import="root.front.dto.DTOSalida"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <title>:)Cumpleaños:)</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.jsp">Inicio<span class="sr-only">(current)</span></a>
                    </li>

                </ul>

            </div>
        </nav>
        <h1>Datos</h1>
        <% DTOSalida persona = (DTOSalida) request.getAttribute("persona");%>
        <table class="table table-striped table-sm">
            <tbody>
                <tr>
                    <td>Nombre: <%= persona.getNombre()%>   </td>
                </tr>
                <tr>
                    <td>Apellido: <%= persona.getApellido()%>    </td>
                </tr>
                <tr>
                    <td>Fecha Nacimiento: <%= persona.getFechaString()%>  </td>
                </tr>
                <tr>
                    <td>Edad: <%= persona.getEdad()%>   </td>
                </tr>
                <tr>
                    <td> <%
                        if (persona.getDiasParaCumpleanio() < 1) {%>
                        <p>  <%=  persona.getPoema()%> </p>
                        <%} else {
                        %>
                        <p> Faltan <%=  persona.getDiasParaCumpleanio()%> días para tu próximo cumpleaños </p>
                        <%}
                        %>
                    </td>
                </tr>

            </tbody>
        </table>

    </body>
    <footer class="page-footer font-small cyan darken-3">
        <div class="footer-copyright text-center py-3">© 2020 Copyright:
            <a href="https://www.linkedin.com/in/yeseniadoria/"> Yesenia Doria</a><br>
            <a href="https://bitbucket.org/yeseniadorial/cumpleanos"> Código App en Bitbucket</a><br>
            <a href="https://dias-cumple.herokuapp.com/"> App desplegada en Heroku</a>
            
        </div>

    </footer>

</html>
